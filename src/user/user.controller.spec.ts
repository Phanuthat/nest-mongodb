import { Test, TestingModule } from '@nestjs/testing';
import { User } from '../user/models/user.model';
import { UserController } from './user.controller';
import { UserService } from './user.service';

describe('UserController', () => {
  let controller: UserController;
  let service: UserService;
  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      controllers: [UserController],
      providers: [UserService],
    }).compile();
    // service = module.get<UserService>(UserService);
    controller = module.get<UserController>(UserController);
  });

  describe('getUsers', () => {
    it('should return an array of users', async () => {
      // const result: User = {};
      // result.id = '6011954bfb0ed82944c44caf';
      // result.fullName = 'poopae';
      // result.phone = '0885697543';
      // result.email = 'poopae@gmail.com';
      // jest
      //   .spyOn(service, 'getUserByID')
      //   .mockImplementation(() => Promise.resolve(result));

      expect(await controller.GetuserByID('6011954bfb0ed82944c44caf')).toBe({
        id: '6011954bfb0ed82944c44caf',
        fullName: 'poopae',
        phone: '0885697543',
        emai: 'poopae@gmail.com',
      });
    });
  });
});
