import { Injectable, NotFoundException, Response } from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import { Model } from 'mongoose';
import { User, UserReq } from './models/user.model';
@Injectable()
export class UserService {
  constructor(@InjectModel('User') private readonly userModel: Model<User>) {}

  getUsers = async (): Promise<User[]> => {
    return await this.userModel.find().exec();
  };

  getUserByID = async (id: string): Promise<User> => {
    return await this.userModel.findOne({ _id: id });
  };

  createUser = async (userReqModel: UserReq) => {
    const user = new this.userModel({
      fullName: userReqModel.fullName,
      phone: userReqModel.phone,
      email: userReqModel.email,
    });
    return await user.save();
  };
}
