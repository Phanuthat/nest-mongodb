export interface Response<T> {
  status: number;
  result: {
    message: string;
    data: T;
  };
}

export interface ResponseController<T> {
  result: {
    message: string;
    data?: T;
  };
}
